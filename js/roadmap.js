function checkHandler(e) {
        var i1 = document.getElementById("i1");
        var i2 = document.getElementById("i2");
        var i3 = document.getElementById("i3");
        var i4 = document.getElementById("i4");
        var i5 = document.getElementById("i5");
        var i6 = document.getElementById("i6");
        var i7 = document.getElementById("i7");
        var i8 = document.getElementById("i8");
        var i9 = document.getElementById("i9");
        var i10 = document.getElementById("i10");
        var i11 = document.getElementById("i11");
        var i12 = document.getElementById("i12");
        var i13 = document.getElementById("i13");
      
        var t2 = document.getElementById("t2");
        var t4 = document.getElementById("t4");
        var t6 = document.getElementById("t6");
        var t8 = document.getElementById("t8");
        var t11 = document.getElementById("t11");
        var t12 = document.getElementById("t12");
        var t13 = document.getElementById("t13");
        
        /* Ehtorakenteita aika paljon, voisi hoitaa myös toistorakenteella
         * Mikäli jonkin tietyn rivin laatikko ruksataan, vedetään myös rivi yli.
        */
        if (t2.checked) {
          i2.style.textDecoration = "line-through";
        }
        else {
          i2.style.textDecoration = "none";
        }
        
        if (t4.checked) {
          i4.style.textDecoration = "line-through";
        }
        else {
          i4.style.textDecoration = "none";
        }
        
        if (t6.checked) {
          i6.style.textDecoration = "line-through";
        }
        else {
          i6.style.textDecoration = "none";
        }
        
        if (t8.checked) {
          i8.style.textDecoration = "line-through";
        }
        else {
          i8.style.textDecoration = "none";
        }
        
        if (t11.checked) {
          i11.style.textDecoration = "line-through";
        }
        else {
          i11.style.textDecoration = "none";
        }
        
        if (t12.checked) {
          i12.style.textDecoration = "line-through";
        }
        else {
          i12.style.textDecoration = "none";
        }
        
        if (t13.checked) {
          i13.style.textDecoration = "line-through";
        }
        else {
          i13.style.textDecoration = "none";
        }
      }
      
      document.getElementById("listform").addEventListener("click", checkHandler);
