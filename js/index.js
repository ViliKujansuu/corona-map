const INITIAL_CODES = {
  Brunei: "BRN",
  "Mainland China": "CHN",
  US: "USA",
  Iran: "IRN",
  "South Korea": "KOR",
  "Korea, South": "KOR",
  Korea: "KOR",
  "Taiwan*": "TWN",
  UK: "GBR",
  "United Kingdom": "GBR",
  Czechia: "CZE",
  Russia: "RUS",
  "United Arab Emirates": "UAE",
  Macau: "MAC",
  "North Macedonia": "MKD",
  Venezuela: "VEN",
  Vietnam: "VNM",
  "Cote d'Ivoire": "CIV",
  "West Bank and Gaza": "PSE",
  Kosovo: "KOS",
  "Congo (Kinshasa)": "COD",
  "Congo (Brazzaville)": "COG",
  Tanzania: "TZA",
  Burma: "MMR",
  Syria: "SYR",
  Laos: "LAO",
  Eswatini: "SWZ"
};


var codeMap;
var caseMap;
var realneighbours;
var rawNeighbours;
var times;

(async () => {
	var cases = await getJSON(`${config.baseURL}corona`);
	var countries = await getJSON(`${config.baseURL}countries`);
	rawNeighbours = await getJSON(`${config.baseURL}neighbours`);
	var rawTimes = await getJSON(`${config.baseURL}corona/timeseries`);
	
	realneighbours = mapNeighbours(rawNeighbours);
	
	times = rawTimes;
	
	console.log(times);
	
	codeMap = countryCodeMap(countries, INITIAL_CODES);
	fillDataList(codeMap);

	caseMap = mapCasesWithCountrycodes(cases, codeMap);
	initialcolors(caseMap);
})();

const DEFAULT_FILL = "#EEEEEE";

document.getElementById("country").addEventListener("input", inputHandler);

var countryarray = [];

// Asettaa maan värit edustamaan koronatapauksia ja kuolemia, sivun ladatessa. 
function initialcolors(cases) {
	var mapkeys = Object.keys(cases);
	var object = {}
	for (var i = 0; i < mapkeys.length; i++) {
		var current = cases[mapkeys[i]];
		var confirmed = 0;
		var deaths = 0;
		if (current !== undefined) {
			confirmed = current["confirmed"];
			deaths = current["deaths"];
		}
		var color = getColor(confirmed, deaths);
		object[mapkeys[i]] = color;
	}
	map.updateChoropleth(object);
}

const int = (str) => Number.parseInt(str);

const mapNeighbours = rawNeighbours => {
	var neighbourmap = {};
	for (var i = 0; i < rawNeighbours.length; i++) {
		var neighbours = rawNeighbours[i]["borders"];
		var id = rawNeighbours[i]["alpha3Code"];
		neighbourmap[id] = neighbours;
	}
	console.log(neighbourmap);
	return neighbourmap;
};

const getColor = (confirmed, deaths) => {
  const denominator = confirmed + deaths == 0 ? 1 : confirmed + deaths;
  const nominator = deaths ? deaths : 0;
  const hue = int(240 + 120 * nominator / denominator);
  const saturation = 100; //constant

  let weight = int(7 * Math.log(confirmed + 20 * deaths));
  weight = weight ? (weight > 100 ? 95 : weight) : 0;

  let lightness = 95 - weight;
  lightness = lightness < 0 ? 0 : lightness;
  return `hsl(${hue}, ${saturation}, ${lightness})`;
};


/*
  Peruskartta perustäytöllä.
  Projektio 'mercator' koska se on selkein.
  Responsiivinen koko.
*/
var map = new Datamap({
  scope: 'world',
  element: document.getElementById("mapcontainer"),
  responsive: true,
  projection: 'mercator',
  width: 1000,
  height: 700,
  geographyConfig: {
	  highlightOnHover: false,
	  borderColor: "black",
  },
  fills: {
	defaultFill: DEFAULT_FILL
  }})


function countryClicker(e) {
  var countrycodes = Object.values(codeMap);
  
  // Muuttaa kaikki maat takaisin perusvärille.
  for (var a = 0; a < countrycodes.length; a++) {
	  var object = {};
	  object[countrycodes[a]] = DEFAULT_FILL;
	  map.updateChoropleth(object);
  }
  
  // Löytää maan nimen ja koodin.
  var country = e.target.__data__.properties.name;
  var rowcode = codeMap[country];
  var rows = "";
  
  // Jos maalle löytyy codeMapista vastaava maakoodi, jatketaan.
  if (rowcode !== undefined) {
	
	// Etsitään maan koronakuolemien määrä ja määritellään hsl värin sävy sillä.
	var deaths = 0;
	var confirmed = 0;
	if (caseMap[rowcode] !== undefined) {
		deaths = caseMap[rowcode]["deaths"];
		confirmed = caseMap[rowcode]["confirmed"];
	}
	
	var object = {};
	
	// Luodaan avain-arvo pari käyttäen maakoodia avaimena, ja päivitetään se karttaan
	var color = getColor(confirmed, deaths);
	object[rowcode] = color;
	map.updateChoropleth(object);
	
	// Etsitään maan naapurit ja käydään ne läpi.
	var neighbours = realneighbours[rowcode];
	for (var b = 0; b < neighbours.length; b++) {
		var neighbourid = neighbours[b];
		var neighbourobj = {};
		
		// Samalla tavalla etsitään naapurien kuolemat, ja värjätään maat niiden perusteella.
		var ndeaths = 0;
		var nconfirmed = 0;
		if (caseMap[neighbourid] !== undefined) {
			ndeaths = caseMap[neighbourid]["deaths"];
			nconfirmed = caseMap[neighbourid]["confirmed"];
		}
		var ncolor = getColor(nconfirmed, ndeaths);
		neighbourobj[neighbourid] = ncolor;
		map.updateChoropleth(neighbourobj);
	}
	
	// Lisätään maa taulukkoon ja uudelleenjärjestetään taulukko tarvittaessa.
	var index = countryarray.indexOf(country);
	for (var j = 0; j < countryarray.length; j++) {
		if (country === countryarray[j]) {
			countryarray.splice(index, 1);
			countryarray.sort();
		}
	}
	countryarray.unshift(country);
	for (var i = 0; i < countryarray.length; i++) {
		var finalrowcode = codeMap[countryarray[i]];
		var row = constructTableRow(finalrowcode);
		rows = rows + row;
	}
	var tbody = document.getElementsByTagName("tbody");
	tbody[0].innerHTML = rows;
	}
	else {
		return;
	}
}

// Funktio, jota voidaan käyttää pistämään funktio odottamaan haluttu aika.
function wait(time) {
	return new Promise(resolve => setTimeout(resolve, time));
}

async function timehandler(e) {
	var codes = Object.values(codeMap);
	
	// Muuttaa kaikki maat valkoisiksi.
	for (var a = 0; a < codes.length; a++) {
		var object = {};
		object[codes[a]] = DEFAULT_FILL;
		map.updateChoropleth(object);
	}
	
	// Ottaa tiedot tauluista.
	var confirmedmap = times[0];
	var deathmap = times[1];
	
	// Muodostaa taulut tiedoista.
	var confirmedarray = confirmedmap["confirmed"];
	var deatharray = deathmap["deaths"];
	
	// Luo mapin, johon laitetaan avaimeksi maakoodi ja arvoksi taulu koronavirustapauksista päivässä.
	var confirmedmap = {};
	for (var i = 0; i < confirmedarray.length; i++) {
		
		// Poimitaan yhden maan tiedot taulusta.
		var cconfirmed = confirmedarray[i];
		
		// Jos kyseessä ei ole maahan kuuluva osavaltio, etsitään maakoodi ja asetetaan tiedot uuten mappiin.
		if (cconfirmed["Province/State"] === "") {
			var countryname = cconfirmed["Country/Region"].split(" (");
			var finalname = countryname[0].split("_").join(" ");
			var countrycode = codeMap[finalname];
			
			if (countrycode !== undefined) {
				confirmedmap[countrycode] = cconfirmed;
			}
		}
		
		// Jos kyseessä on osavaltio, lisätään tiedot siihen kuuluvaan maahan.
		else {
			var countryname = cconfirmed["Country/Region"].split(" (");
			var finalname = countryname[0].split("_").join(" ");
			var countrycode = codeMap[finalname];
			
			if (countrycode !== undefined && confirmedmap[countrycode] === undefined) {
				confirmedmap[countrycode] = cconfirmed;
			}
			else if (countrycode !== undefined && confirmedmap[countrycode] !== undefined) {
				var values = Object.values(confirmedmap[countrycode]);
				var vkeys = Object.keys(confirmedmap[countrycode]);
				for (var d = 0; d < vkeys.length; d++) {
					var oldconfirmed = confirmedmap[countrycode][vkeys[d]];
					var newconfirmed = cconfirmed[vkeys[d]] + oldconfirmed;
					confirmedmap[countrycode][vkeys[d]] = newconfirmed; 
				}
			}
		}
		
		// Poistaa tiedot, jotka eivät ole päivämääriä.
		delete cconfirmed["Province/State"];
		delete cconfirmed["Country/Region"];
		delete cconfirmed["Lat"];
		delete cconfirmed["Long"];
	}
	
	console.log(confirmedmap);
	
	// Luo mapin, johon laitetaan avaimeksi maakoodi ja arvoksi taulu koronaviruskuolemista päivässä.
	var deathmap = {};
	for (var j = 0; j < deatharray.length; j++) {
		
		// Poimitaan yhden maan tiedot taulusta.
		var cdeaths = deatharray[j];
		
		// Jos kyseessä ei ole maahan kuuluva osavaltio, etsitään maakoodi ja asetetaan tiedot uuten mappiin.
		if (cdeaths["Province/State"] === "") {
			var countryname = cdeaths["Country/Region"].split(" (");
			var finalname = countryname[0].split("_").join(" ");
			var countrycode = codeMap[finalname];
			
			if (countrycode !== undefined) {
				deathmap[countrycode] = cdeaths;
			}
		}
		
		// Jos kyseessä on osavaltio, lisätään tiedot siihen kuuluvaan maahan.
		else {
			var countryname = cdeaths["Country/Region"].split(" (");
			var finalname = countryname[0].split("_").join(" ");
			var countrycode = codeMap[finalname];
			
			if (countrycode !== undefined && deathmap[countrycode] === undefined) {
				deathmap[countrycode] = cdeaths;
			}
			else if (countrycode !== undefined && deathmap[countrycode] !== undefined) {
				var values = Object.values(deathmap[countrycode]);
				var vkeys = Object.keys(deathmap[countrycode]);
				var loop = vkeys.length;
				for (var d = 0; d < loop; d++) {
					var olddeaths = deathmap[countrycode][vkeys[d]];
					var newdeaths = cdeaths[vkeys[d]] + olddeaths;
					deathmap[countrycode][vkeys[d]] = newdeaths; 
				}
			}
		}
		delete cdeaths["Province/State"];
		delete cdeaths["Country/Region"];
		delete cdeaths["Lat"];
		delete cdeaths["Long"];
	}
	
	var countrycodes = Object.keys(confirmedmap);
	var dates = Object.keys(confirmedmap["AFG"]);
	var currentdate = document.getElementById("date");
	
	for (var b = 0; b < dates.length; b++) {
		
		var object = {};
		currentdate.innerText = dates[b];
		
		for (var c = 0; c < countrycodes.length; c++) {
			
			var confirmed = confirmedmap[countrycodes[c]][dates[b]];
			var deaths = deathmap[countrycodes[c]][dates[b]];
			
			if (confirmed === undefined) {
				confirmed = 0;
			}
			
			if (deaths === undefined) {
				deaths = 0;
			}
			
			var color = getColor(confirmed, deaths);
			
			object[countrycodes[c]] = color;
		}
		await wait(1000);
		map.updateChoropleth(object);
	}
	times = await getJSON("https://tie-lukioplus.rd.tuni.fi/corona/api/corona/timeseries");
}

// Koon muuttaja.
window.addEventListener('resize', function() {
  map.resize();
});

document.getElementById("mapcontainer").addEventListener('click', countryClicker);

document.getElementById("timeseries").addEventListener("click", timehandler);

function getKey(object, value) {
    var keys = Object.keys(object);
    var key = "";
    for (var a = 0; a < keys.length; a++) {
        var keyvalue = object[keys[a]];
        if (keyvalue === value) {
            key = keys[a];
        }
    }
    if (key === "") {
        return null;
    }
    else {
        return key;
    }
}

function constructTableRow(code) {
    var countrystats = caseMap[code];
    var row = "";
    if (countrystats !== undefined) {
		var country = caseMap[code]["country"];
        var cv = Object.values(countrystats);
        row = "<tr><td>" + country + "</td><td>" + cv[0] + "</td><td>" + cv[1] + "</td><td>" + cv[2] + "</td></tr>";
        return row;
    }
    else {
        var country = getKey(codeMap, code);
        if (country !== null) {
            row = "<tr><td>" + country + "</td><td>" + "-" + "</td><td>" + "-" + "</td><td>" + "-" + "</td></tr>";
        }
        return row;
    }
}

function getJSON(url) {
	return fetch(url)
		.then(function(response) {
			return response.json();
		});
}

function countryCodeMap(countries, initialCodes) {
	var cmap = {};
	var initialkeys = Object.keys(initialCodes);
	var initialvalues = Object.values(initialCodes);
	for (var j = 0; j < initialkeys.length; j++) {
		cmap[initialkeys[j]] = initialvalues[j];
	}
	for (var i = 0; i < countries.length; i++) {
		var key = countries[i]["name"];
		var keyparts = key.split(" (");
		var finalkey = keyparts[0];
		var value = countries[i]["alpha3Code"];
		cmap[finalkey] = value;
	}
	return cmap;
}


function fillDataList(codeMap) {
	var ckeys = Object.keys(codeMap);
	ckeys.sort();
	var content = "";
	for (var i = 0; i < ckeys.length; i++) {
		var row = "<option value=\"" + ckeys[i] + "\">";
		content = content + row;
	}
	var dl = document.getElementById("searchresults");
	dl.innerHTML = content;
}

function mapCasesWithCountrycodes(cases, countries) {
	var casekeys = Object.keys(cases);
	var finalmap = {};
	for (var i = 0; i < casekeys.length; i++) {
		var realkey = casekeys[i].split("_").join(" ");
		var ccase = cases[casekeys[i]];
		var value = countries[realkey];
		if (ccase !== undefined) {
			finalmap[value] = ccase;
			finalmap[value]["country"] = realkey;
		}
	}
	return finalmap;
}

function inputHandler(e) {
	
	// Hakee tekstikentän sisällön.
	var inputfield = document.getElementById("country");
	var input = inputfield.value;
	
	// Hakee maakoodin.
	var rowcode = codeMap[input];
	var rows = "";
	var countrycodes = Object.values(codeMap);
	
	// Muuttaa kaikki maat takaisin perusvärille.
	for (var a = 0; a < countrycodes.length; a++) {
	  var object = {};
	  object[countrycodes[a]] = DEFAULT_FILL;
	  map.updateChoropleth(object);
	}
	
	// Jos maakoodi löytyy, jatketaan.
	if (rowcode !== undefined) {
		
		// Etsitään maassa tapahtuneiden koronakuolemien määrä ja asetetaan hsl-värin kirkkaus sen perusteella.
		var deaths = 0;
		var confirmed = 0;
		if (caseMap[rowcode] !== undefined) {
			deaths = caseMap[rowcode]["deaths"];
			confirmed = caseMap[rowcode]["confirmed"];
		}
		
		var object = {};
		var color = getColor(confirmed, deaths);
		// Muodostetaan avain-arvo pari maakoodin ja hsl-värin välille ja päivitetään väri karttaan.
		object[rowcode] = color;
		map.updateChoropleth(object);
		
		// Etsitään maan naapurit ja käydään ne läpi.
		var neighbours = realneighbours[rowcode];
		for (var b = 0; b < neighbours.length; b++) {
			var neighbourid = neighbours[b];
			var neighbourobj = {};
		
			// Samalla tavalla etsitään naapurien kuolemat, ja värjätään maat niiden perusteella.
			var ndeaths = 0;
			var nconfirmed = 0;
			if (caseMap[neighbourid] !== undefined) {
				ndeaths = caseMap[neighbourid]["deaths"];
				nconfirmed = caseMap[neighbourid]["confirmed"];
			}
			var ncolor = getColor(nconfirmed, ndeaths);
			neighbourobj[neighbourid] = ncolor;
			map.updateChoropleth(neighbourobj);
		}
		
		// Tyhjennetään tekstikenttä.
		inputfield.value = null;
		
		// Lisätään rivi taulukkoon tai järjestellään taulukko uudelleen, jos rivi on jo taulukossa.
		var index = countryarray.indexOf(input);
		for (var j = 0; j < countryarray.length; j++) {
			if (input === countryarray[j]) {
				countryarray.splice(index, 1);
				countryarray.sort();
			}
		}
		countryarray.unshift(input);
		for (var i = 0; i < countryarray.length; i++) {
			var finalrowcode = codeMap[countryarray[i]];
			var row = constructTableRow(finalrowcode);
			rows = rows + row;
		}
		var tbody = document.getElementsByTagName("tbody");
		tbody[0].innerHTML = rows;
	}
	else {
		return;
	}
}
